import requests
import json

r = requests.get('https://api.deezer.com/user/1526206986/playlists')
playlists_data = json.loads(r.text)

delimiter = 40*'-'

print(delimiter)
for playlist in playlists_data['data']:
    print('playlist: {}, created by: {}'.format(playlist['title'],
                                                playlist['creator']['name']))
print(delimiter)
