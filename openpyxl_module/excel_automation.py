from openpyxl import load_workbook

wb = load_workbook('./Financial Sample.xlsx')

sheet = wb["Finances 2017"]

max_row = sheet.max_row

sheet["L{}".format(max_row)] = "=SUM(L2:L{})".format(max_row-1)

wb.save("./Financial Sample.xlsx")

total = 0

for v in range(2, max_row):
    value = sheet["L{}".format(v)].value
    if value:
        total += value

print(total)
