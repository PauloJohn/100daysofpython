import csv
from collections import defaultdict, Counter, namedtuple


def get_directors():
    Movie = namedtuple('Movie', 'movie_title title_year imdb_score')
    directors = defaultdict(list)
    with open('./movie_metadata.csv', 'r') as f:
        movie_data = csv.DictReader(f)
        for row in movie_data:
            if row.get('title_year'):
                if int(row.get('title_year')) >= 1960:
                    directors[row['director_name']].append(
                        Movie(
                            row.get('movie_title'),
                            int(row.get('title_year')),
                            row.get('imdb_score'),
                        ))
    return directors


directors = get_directors()
cnt = Counter()
for director, movies in directors.items():
    cnt[director] += len(movies)

print(cnt.most_common(5))
