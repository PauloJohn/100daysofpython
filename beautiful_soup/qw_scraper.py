import requests
import bs4 as bs

URL = 'http://queroworkar.com.br/blog/'


def get_website(url):
    user_agent = {'User-agent': 'Mozilla/5.0'}
    website = requests.get(url, headers=user_agent)
    website.raise_for_status()

    return website.text


def get_jobs_available(site):
    soup = bs.BeautifulSoup(site, 'html.parser')
    return [element.text for element in soup.select("h4 > a")]


if __name__ == '__main__':
    from pprint import pprint as pp
    website = get_website(URL)
    pp(get_jobs_available(website))
