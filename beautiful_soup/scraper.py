#!/bin/env python

import requests
import bs4

URL = "https://pybit.es/pages/projects.html"


def pull_site():
    raw_site_page = requests.get(URL)
    raw_site_page.raise_for_status()
    return raw_site_page


def scrape(site):
    soup = bs4.BeautifulSoup(site.text, 'html.parser')
    html_header_list = soup.select('h3.projectHeader')

    return [headers.getText() for headers in html_header_list]


if __name__ == '__main__':
    from pprint import pprint as pp
    pp(scrape(pull_site()))
