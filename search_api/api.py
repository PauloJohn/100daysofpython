import requests
from collections import namedtuple

Episode = namedtuple('Episode',
                     ['category', 'id', 'url', 'title', 'description'])


def search_by_keyword(keyword):
    response = requests.get(
        'http://search.talkpython.fm/api/search?q={}'.format(keyword))
    response.raise_for_status()

    return {get_episode(episode) for episode in response.json().get('results')}


def get_episode(episode_dict):
    return Episode(**episode_dict)
