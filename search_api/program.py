import api
import sys
import webbrowser

args = '-'.join(sys.argv[1:])
if args:
    print("**** Talk Python Search ****")
    print("searching but the keyworkds: {}".format(args))
    episodes = api.search_by_keyword(args)

    print("We've found {episodes} episodes!".format(episodes=len(episodes)))

    for result in episodes:
        print("id: {}, {}".format(result.id, result.title))

    chosen_episode = int(input("type the id for the episode you want:"))

    episode = (ep for ep in episodes if ep.id == chosen_episode).__next__()

    webbrowser.open('https://talkpython.fm{}'.format(episode.url), new=2)

else:
    print('you must pass at least one arg')
