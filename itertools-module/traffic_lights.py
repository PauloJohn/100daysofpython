import sys
from itertools import cycle
from time import sleep
import logbook

app_log = logbook.Logger('App')

lights = cycle([
    {
        'color': "\033[1;32;40mⒼ \033[0;0m",
        'time': 3,
        'name': 'green',
    },
    {
        'color': "\033[1;33;40mⓎ \033[0;0m",
        'time': 1,
        'name': 'yellow',
    },
    {
        'color': "\033[1;31;40mⓇ \033[0;0m",
        'time': 3,
        'name': 'red',
    },
])


def light_rotation(lights):
    for color in lights:
        sys.stdout.write("\r")
        sys.stdout.write(color.get('color'))
        app_log.notice('turned {}'.format(color.get('name')))
        sys.stdout.flush()
        sleep(color.get('time'))


def init_logging(log_file=None, level=logbook.WARNING):
    if log_file:
        logbook.TimedRotatingFileHandler(log_file).push_application()
    else:
        logbook.StreamHandler(sys.stdout).push_application()


if __name__ == '__main__':
    init_logging(log_file='semaphore.log', level=logbook.NOTICE)
    app_log.notice('initiated semaphore')
    light_rotation(lights)
