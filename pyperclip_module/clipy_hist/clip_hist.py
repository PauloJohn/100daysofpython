import pyperclip
from datetime import datetime


def append_content_to_file(clipboard_content, filename):
    with open(filename, 'a') as f:
        f.writelines('{}:\n\n'.format(datetime.now().isoformat()))
        f.writelines('{}\n'.format(clipboard_content))
        f.writelines('{}\n'.format(40*'-'))


if __name__ == '__main__':

    clipboard_content = ""
    filename = "/tmp/clipboard_history.txt"

    while True:
        if clipboard_content != pyperclip.paste():
            clipboard_content = pyperclip.paste()
            append_content_to_file(clipboard_content, filename)
