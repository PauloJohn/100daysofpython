#!/usr/bin/env python

"""
Small script that gets the stream from a linux pip
and pastes to the clipboard
"""

import pyperclip
import sys

text = sys.stdin.read()

pyperclip.copy(text)

print(text)
