from functools import wraps
from colors import color


def format(color):
    def inner_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return "{}{}{}".format(color, func(*args, **kwargs),
                                   '\033[0m')
        return wrapper
    return inner_decorator


@format(color.ITALIC)
@format(color.UNDERLINE)
@format(color.BOLD)
def say_something(something):
    return something


print(say_something("Hello World!"))
print(say_something("Something Else!"))
