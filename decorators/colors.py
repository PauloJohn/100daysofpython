from collections import namedtuple
Color = namedtuple('Color', ['BOLD', 'ITALIC', 'UNDERLINE'])
color = Color('\033[1m', '\033[3m', '\033[4m', )
