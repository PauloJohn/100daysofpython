class Roll:
    def __init__(self, name, defeats, defeated_by):
        self.name = name
        self.defeats = defeats
        self.defeated_by = defeated_by

    def can_defeat(self, enemy_roll):
        return enemy_roll.name in self.defeats

    def __repr__(self):
        return self.name


class Player:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name
