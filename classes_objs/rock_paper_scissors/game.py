from elements import Player, Roll
from collections import Counter
import random


def print_header():
    print(40 * '-')
    print(5 * ' ', 'Rock, Paper and Scissors Game')
    print(40 * '-')


def build_the_three_rolls():
    paper = Roll(name='paper', defeats=('rock', ), defeated_by=('scissors', ))
    rock = Roll(name='rock', defeats=('scissors', ), defeated_by=('paper', ))
    scissors = Roll(
        name='scissors', defeats=('paper', ), defeated_by=('rock', ))

    return [paper, rock, scissors]


def get_players_name():
    return input("Type your name: ")


def game_loop(player1, player2, rolls):
    count = 1
    winners_count = Counter()
    while count < 10:
        p2_roll = random.choice(rolls)
        try:
            p1_roll = rolls[int(
                input('type 0 for paper, 1 for rock or 2 or scissors: ')
            )]
        except (IndexError, ValueError):
            print("you should chose 0(paper),1(rock) or 2(scissors)")
            continue

        outcome = p1_roll.can_defeat(p2_roll)

        # display throws
        print(40 * '-')
        print(f'{player1.name} choose {p1_roll}')
        print(f'{player2.name} choose {p2_roll}')
        print(40 * '-')

        # display winner for this round
        if outcome:
            print(f'------------{player1.name} WON!!-------')
            winners_count[player1.name] += 1
        elif p2_roll.name == p1_roll.name:
            print(f'---------------TIDE!!-------------------')
        else:
            winners_count[player2.name] += 1
            print(f'------------{player2.name} WON!!-------')

        count += 1

    # Compute who won
    print(f'the absolute winner is {winners_count.most_common(1)[0][0]}')


def main():
    print_header()

    rolls = build_the_three_rolls()

    name = get_players_name()

    player1 = Player(name)
    player2 = Player("computer")

    game_loop(player1, player2, rolls)


if __name__ == '__main__':
    main()
