from elements import Roll, Player
import pytest


def test_instantiate_roll():
    roll = Roll(name='paper', defeats=['rock'], defeated_by=['scissors'])
    assert (roll.name, roll.defeats, roll.defeated_by) == ('paper', ['rock'],
                                                           ['scissors'])
    assert roll.__repr__() == 'paper'


def test_instantiate_name():
    player = Player(name='player1')
    assert player.name == 'player1'
    assert player.__repr__() == 'player1'


def test_can_defeat(tree_rolls):
    assert tree_rolls.get('paper').can_defeat(tree_rolls.get('rock'))
    assert tree_rolls.get('rock').can_defeat(tree_rolls.get('scissors'))
    assert tree_rolls.get('scissors').can_defeat(tree_rolls.get('paper'))


@pytest.fixture()
def tree_rolls():
    rolls = {
        'paper': Roll(
            name='paper', defeats=['rock'], defeated_by=['scissors']),
        'rock': Roll(name='rock', defeats=['scissors'], defeated_by=['paper']),
        'scissors': Roll(
            name='scissors', defeats=['paper'], defeated_by=['rock'])
    }
    return rolls
