from github import Github
from repo_operations import create_repo, delete_repo
import os
import argparse

# Settings
token = os.environ['GIT_REPO_TOKEN']
gh = Github(token)
me = gh.get_user()
###

parser = argparse.ArgumentParser()
parser.add_argument("repo_name", type=str,
                    help="The name of the repo")
parser.add_argument("-d", "--delete", help="deletes a repo",
                    action="store_true")
parser.add_argument("-c", "--create", help="create a new repo",
                    action="store_true")
parser.add_argument("-p", "--private", help="this is a private repo",
                    action="store_true")
args = parser.parse_args()


if args.create:
    created_repo = create_repo(me, args.repo_name, args.private)
    print('repository {} created successfully'.format(args.repo_name))
    print(created_repo.clone_url)
    print(created_repo.ssh_url)

if args.delete:
    delete_repo(me, args.repo_name, args.private)
    print('repository {} deleted successfully'.format(args.repo_name))
