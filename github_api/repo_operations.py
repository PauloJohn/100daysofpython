
def create_repo(user, repo_name, private):
    return user.create_repo(repo_name, private=private)


def delete_repo(user, repo_name, private):
    repo = user.get_repo(repo_name)
    repo.delete()
