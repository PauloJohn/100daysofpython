import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os

from_addr = 'paulojhonatas2011@gmail.com'
to_addr = 'pjonathasalves@gmail.com'
bcc = ['pjonathasalves@gmail.com', 'myemail@gmail.com', 'email@gmail.com']

msg = MIMEMultipart()
msg['From'] = from_addr
msg['To'] = to_addr
msg['Subject'] = 'Testing sending automatic emails'

body = """Automatic email test
"""

msg.attach(MIMEText(body, 'plain'))

smtp_server = smtplib.SMTP('smtp.gmail.com', 587)

smtp_server.ehlo()

smtp_server.starttls()

smtp_server.login(' paulojhonatas2011@gmail.com ', ' {} '.format(
    os.environ['GOOGLE_PASS']))

text = msg.as_string()

smtp_server.sendmail(from_addr, [to_addr] + bcc, text)

smtp_server.quit()

print('Email sent successfully')
