from datetime import datetime, timedelta
from models.times import Times
from data import session_factory


class WorkHoursCalc:
    session = session_factory.create_session()
    hours_of_work_per_day = timedelta(hours=8)

    def __init__(self, first_time, second_time, third_time):
        self.first_time = datetime.strptime(first_time, "%I:%M")
        self.second_time = datetime.strptime(second_time, "%I:%M")
        self.third_time = datetime.strptime(third_time, "%I:%M")

        times = Times()
        times.time1 = self.first_time
        times.time2 = self.second_time
        times.time3 = self.third_time
        WorkHoursCalc.session.add(times)
        WorkHoursCalc.session.commit()

    def calc_finish_time(self):
        number_of_hours_before_interval = self.second_time - self.first_time
        time_remaining = WorkHoursCalc.hours_of_work_per_day - number_of_hours_before_interval
        finish_time = self.third_time + time_remaining
        return finish_time.strftime("%H:%M")
