from worktimecalc import WorkHoursCalc


if __name__ == '__main__':

    print("The time should be in the format: 00:00")
    first_time = input("First time: ")
    second_time = input("Second time: ")
    third_time = input("Third time: ")
    try:
        workcalc = WorkHoursCalc(first_time, second_time, third_time)
        print("You should go home by:", workcalc.calc_finish_time())

    except ValueError:
        msg = "At least one of your times is in the incorrect format!"
        print(msg)
