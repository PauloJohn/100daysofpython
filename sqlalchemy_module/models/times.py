from models.base import SqlAlchemyBase
import sqlalchemy


class Times(SqlAlchemyBase):
    __tablename__ = 'times'

    id = sqlalchemy.Column(
        sqlalchemy.Integer, primary_key=True, autoincrement=True)
    time1 = sqlalchemy.Column(sqlalchemy.DateTime)
    time2 = sqlalchemy.Column(sqlalchemy.DateTime)
    time3 = sqlalchemy.Column(sqlalchemy.DateTime)
    time4 = sqlalchemy.Column(sqlalchemy.DateTime)
