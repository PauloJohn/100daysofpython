import feedparser
import sys

feed = feedparser.parse('new_on_netflix.yml')
entries = feed.get('entries')[:4]

keys = sys.argv[1:]

for entry in entries:
    for key in keys:
        print("{}: {}".format(key, entry.get(key)))
    print('------------------------------------------')
