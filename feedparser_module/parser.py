#!/bin/env python3

import feedparser

FEED_FILE = "newreleases.xml"

feed = feedparser.parse(FEED_FILE)

for entry in feed.entries:
    print("""
published: {}
title: {}
link: {}""".format(entry.published, entry.title, entry.link))
