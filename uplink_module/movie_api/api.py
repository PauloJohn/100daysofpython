import uplink


@uplink.json
class MovieServiceClient(uplink.Consumer):
    def __init__(self):
        super().__init__(
            base_url='http://movie_service.talkpython.fm/')

    @uplink.get('/api/search/{keyword}')
    def movies_by_keyword(self, keyword):
        """ Get movies that match with keyword. """

    @uplink.get('/api/director/{director_name}')
    def movies_by_director(self, director_name):
        """ Get movies by director's name. """

    @uplink.get('/api/movie/{imdb_number}')
    def movies_by_imdb_code(self, imdb_number):
        """ Get movies by imdb code. """
