import api
from pprint import pprint as pp


def get_api_response(resquest_func, input_message):
    response = resquest_func(input(input_message))
    response.raise_for_status()
    return response.json()


def main():
    option = True

    while option:
        print('Get movie by [d]irector, [k]eyword, [i]mdb code:')
        option = input('type your option: ')

        consumer = api.MovieServiceClient()

        if option == 'd':
            response = get_api_response(consumer.movies_by_director,
                                        "type the director's name: ")
        elif option == 'k':
            response = get_api_response(consumer.movies_by_keyword,
                                        "type the keyword you want: ")
        elif option == 'i':
            response = get_api_response(consumer.movies_by_imdb_code,
                                        "type the imdb code: ")

        print()
        pp(response)
        print()


if __name__ == '__main__':
    main()
