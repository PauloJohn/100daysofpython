import uplink
import requests

from uplink_helpers import raise_for_status


@uplink.json
@raise_for_status
class BlogClient(uplink.Consumer):
    def __init__(self):
        super().__init__(
            base_url='http://consumer_services_api.talkpython.fm/')

    @uplink.get('/api/blog')
    def all_entries(self) -> requests.models.Response:
        """ Get's all blog entries from the server. """

    @uplink.get('/api/blog/{post_id}')
    def entry_by_id(self, post_id) -> requests.models.Response:
        """ Get single post details. """

    @uplink.post('/api/blog')
    def create_new_entrie(self,
                          **kwargs: uplink.Body) -> requests.models.Response:
        """ Creates a new post. """
