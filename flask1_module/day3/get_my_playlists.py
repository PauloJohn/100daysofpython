import requests
import json

r = requests.get('https://api.deezer.com/user/1526206986/playlists')
playlists_data = json.loads(r.text)

playlists = [{
    "title": playlist['title'],
    "creator": playlist['creator']['name']
} for playlist in playlists_data['data']]

if __name__ == '__main__':
    print(playlists)
