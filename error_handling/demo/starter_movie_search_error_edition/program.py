import api
from requests import exceptions


def main():
    keyword = 'Python' # input('Keyword of title search: ')
    try:
        results = api.find_movie_by_title(keyword)
    except exceptions.ConnectionError:
        print("ERROR: Please verify your connection!")
        return

    print(f'There are {len(results)} movies found.')
    for r in results:
        print(f"{r.title} with code {r.imdb_code} has score {r.imdb_score}")


if __name__ == '__main__':
    main()
