from collections import namedtuple
from worktimecalc import WorkHoursCalc

Time = namedtuple('Time', 'first second third')


def test_worktimecalc():
    time = Time(first='08:00', second='11:30', third='12:00')

    workcalc = WorkHoursCalc(time.first, time.second, time.third)
    assert workcalc.calc_finish_time() == '04:30'
