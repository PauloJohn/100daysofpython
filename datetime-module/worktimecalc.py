from datetime import datetime, timedelta
import logbook

worktime_log = logbook.Logger('WorkTime')


class WorkHoursCalc:
    hours_of_work_per_day = timedelta(hours=8)

    def __init__(self, first_time, second_time, third_time):
        worktime_log.notice('calling constructor')
        self.first_time = datetime.strptime(first_time, "%I:%M")
        self.second_time = datetime.strptime(second_time, "%I:%M")
        self.third_time = datetime.strptime(third_time, "%I:%M")

    def calc_finish_time(self):
        number_of_hours_before_interval = self.second_time - self.first_time
        time_remaining = WorkHoursCalc.hours_of_work_per_day - number_of_hours_before_interval
        finish_time = self.third_time + time_remaining
        return finish_time.strftime("%H:%M")
