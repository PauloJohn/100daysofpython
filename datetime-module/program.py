import sys
from worktimecalc import WorkHoursCalc

import logbook

app_log = logbook.Logger('App')


def init_logging(log_filename=None, level=logbook.WARNING):

    if not log_filename:
        logbook.StreamHandler(sys.stdout, level=level).push_application()
    else:
        logbook.TimedRotatingFileHandler(
            log_filename, level=level).push_application()


if __name__ == '__main__':
    init_logging(log_filename='worktime.log', level=logbook.NOTICE)
    app_log.notice('Log Initialized')

    print("The time should be in the format: 00:00")
    first_time = input("First time: ")
    second_time = input("Second time: ")
    third_time = input("Third time: ")
    try:
        workcalc = WorkHoursCalc(first_time, second_time, third_time)
        print("You should go home by:", workcalc.calc_finish_time())
    except ValueError:
        msg = "At least one of your times is in the incorrect format!"
        print(msg)
        app_log.warn(msg)
    app_log.notice('finishing')
