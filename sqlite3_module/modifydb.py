import sqlite3


def main():
    while True:
        get_all_entries()
        rowid = input("enter the rowid of the entrie you want to alter(blank to quit): ")
        if rowid:
            alter_entry(rowid)
        else:
            break


def get_all_entries():
    print("-------------------Entries--------------")
    with sqlite3.connect("addressbook.db") as connection:
        c = connection.cursor()
        c.execute("SELECT rowid, * FROM Details")

        for row in c.fetchall():
            print("rowid: {}, name: {}, adress: {}, phone: {}".format(*row))


def alter_entry(rowid):
    print()
    with sqlite3.connect("addressbook.db") as connection:
        c = connection.cursor()
        c.execute("SELECT * FROM Details WHERE rowid = ?", rowid)
        row = list(c.fetchone())
        name = input("Enter a new name(blank if you want to keep):")
        address = input("Enter a new address(blank if you want to keep):")
        phone_number = input("Enter a new phone_number(blank if you want to keep):")

        if name:
            row[0] = name
        if address:
            row[1] = address
        if phone_number:
            row[2] = phone_number
        row.append(rowid)

        c.execute(
            "UPDATE Details SET name = ?, address = ?, phone_number = ? WHERE rowid = ?",
            row)


if __name__ == '__main__':
    main()
