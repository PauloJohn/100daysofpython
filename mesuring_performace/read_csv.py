import os
import csv
current_dir = os.path.dirname(os.path.realpath(__file__))


def return_list_heroes():
    with open(os.path.join(current_dir, 'data/marvel-wikia-data.csv'),
              'r') as f:
        rows = csv.DictReader(f)

        for row in rows:
            yield normalize_data(row)


def normalize_data(row):
    row['ALIVE'] = row['ALIVE'] == 'Living Characters'

    return row


def return_male_heroes():
    return [
        hero for hero in return_list_heroes()
        if hero.get('SEX') == 'male'
    ]


def return_female_heroes():
    return [
        hero for hero in return_list_heroes()
        if hero.get('SEX') == 'Female Characters'
    ]


def return_dead_characters():
    return [hero for hero in return_list_heroes() if not hero.get('ALIVE')]


def print_formated_hero_table(heroes_list):
    print('NAME | SEX')

    for hero in heroes_list:
        print('{}|{}'.format(hero.get('name'), hero.get('SEX')))
