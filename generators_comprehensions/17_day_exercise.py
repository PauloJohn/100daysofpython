import random
NAMES = [
    'arnold schwarzenegger', 'alec baldwin', 'bob belderbos',
    'julian sequeira', 'sandra bullock', 'keanu reeves', 'julbob pybites',
    'bob belderbos', 'julian sequeira', 'al pacino', 'brad pitt', 'matt damon',
    'brad pitt'
]

T_NAMES = [name.title() for name in NAMES]
print(T_NAMES)


def gen_pairs():
    while True:
        yield "{} times up with {}".format(
            random.choice(T_NAMES).split()[0],
            random.choice(T_NAMES).split()[0])


pairs = gen_pairs()
for _ in range(10):
    print(next(pairs))
