from selenium import webdriver
import unittest

URL = "http://pyplanet.herokuapp.com/"
user = "guest"
pw = "changeme"


class SeleniumSite(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_website(self):
        # 1
        self.driver.get(URL)
        assert self.driver.find_element_by_css_selector('h1').text == 'PyBites 100 Days of Django'
        # 2
        self.driver.find_element_by_css_selector('a[href=pyplanet').click()
        # 3
        self.driver.find_element_by_css_selector('.pure-table a').click()
        self.driver.find_element_by_css_selector('.pure-button').click()
        # 4
        self.driver.find_element_by_css_selector("a[href=\/login\/").click()
        self.driver.find_element_by_id("id_username").send_keys(user)
        self.driver.find_element_by_id("id_password").send_keys(pw)
        self.driver.find_element_by_css_selector("button[type=submit]").click()
        # 5
        self.driver.find_element_by_css_selector('a[href=pyplanet').click()
        self.driver.find_element_by_css_selector('.pure-table a').click()

        # 7
        self.driver.find_element_by_css_selector("a[href=\/logout\/").click()

    def tearDown(self):
            self.driver.close()


if __name__ == "__main__":
    unittest.main()
